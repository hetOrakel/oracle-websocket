const oradb = require('./dbconfig')
var os = require("os");
const hostname = os.hostname();
const WebSocket = require('ws')
const wss = new WebSocket.Server({ port: 18080 })

wss.getUniqueID = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
    }
    return s4() + s4() + '-' + s4() + s4() + '-' + s4() + s4() 
};

let db_send_conn; 
let db_read_conn; 

async function db_connect() {
    try {
        db_send_conn = await oradb.db.getConnection( {
            user          : oradb.settings.user,
            password      : oradb.settings.password,
            connectString : oradb.settings.connectString
        });    
        db_read_conn = await oradb.db.getConnection( {
            user          : oradb.settings.user,
            password      : oradb.settings.password,
            connectString : oradb.settings.connectString
        });    
        // give some info to the dba:
        let result
        result = await db_send_conn.execute
            (   "BEGIN  dbms_application_info.set_client_info( '" + hostname + " websocket send connection');  END;"
            ,   { 
            })
        result = await db_read_conn.execute
            (   "BEGIN  dbms_application_info.set_client_info( '" + hostname + " websocket read connection');  END;"
            ,   { 
            })
        db_read(hostname)
    } catch (err) {
        console.error(err)
    } finally {
        // if (db_connection) {
        //     try {
        //         await db_connection.close()
        //     } catch (err) {
        //         console.error(err)
        //     }
        // }
    }   
}

async function db_send(id,servername,message) {
    const result = await db_send_conn.execute
        ( `BEGIN  aqenq('todb',:p_sender, :p_servername, :p_message); END;`
        , { p_sender: id,  p_servername: servername, p_message: message, 
        })          
}

async function db_read(servername) {
    while(true){
        const result = await db_read_conn.execute
            (   `BEGIN  aqdeq('tows',:p_sender, :p_servername, :p_message); END;`
            ,   { p_sender:     { dir: oradb.db.BIND_OUT, type: oradb.db.STRING, maxSize: 40 }
                , p_servername: servername
                , p_message:    { dir: oradb.db.BIND_OUT, type: oradb.db.STRING, maxSize: 400 }
            })
        wss.clients.forEach(function each(client) {
            if (client.readyState === WebSocket.OPEN && client.id  === result.outBinds.p_sender) {
                console.log('send to : %s %s %s', client.id, result.outBinds.p_servername, result.outBinds.p_message)                 
                client.send(result.outBinds.p_message)
            }})        
    }
}

wss.on('connection', function connection(ws, request, client) {
    console.log(request)    // just for the fun, peeking in info
    ws.id = wss.getUniqueID()
    ws.send('verbinding opgezet')
    ws.on('close', function close() { console.log('close') })   
    ws.on('message', function incoming(message) {
        console.log('received: %s %s', ws.id, message)
        db_send(ws.id,hostname,message)
    })
})

db_connect()
process.on('SIGINT', function() {
    console.log( "\nGracefully shutting down from SIGINT (Ctrl-C)" );
    // some other closing procedures go here
    process.exit(1);
  });