
grant aq_administrator_role, aq_user_role to <USER>;
grant execute on dbms_aq to <USER>;
execute dbms_aqadm.grant_type_access('<USER>');
-- grant create type access to <USER>;

exec dbms_aqadm.grant_system_privilege 
    ( privilege     => 'MANAGE_ANY' 
    , grantee       => '<USER>' 
    , admin_option  => false
    );
exec dbms_aqadm.grant_system_privilege('ENQUEUE_ANY','<USER>',FALSE);
exec dbms_aqadm.grant_system_privilege('DEQUEUE_ANY','<USER>',FALSE);




begin
  dbms_aqadm.stop_queue
    ( queue_name      => 'websocket_queue'
    , enqueue         => true
    , dequeue         => true
    , wait            => false
    );
end;
/
begin
  dbms_aqadm.drop_queue 
    ( queue_name      => 'websocket_queue'
    , auto_commit     => true
    );    
end;
/
begin
  dbms_aqadm.drop_queue_table 
    ( queue_table     => 'websocket_queue_tab'
    , force           => true
    );
end;
/

create or replace type websocket_msg_type as object
  (  direction       varchar2(4)  --tows TO WebSockey todb TO DataBase
  ,  sender          varchar2(40)
  ,  servername      varchar2(40)
  ,  message         clob
  );
/

begin
  dbms_aqadm.create_queue_table  
    ( queue_table         => 'websocket_queue_tab' 
    , queue_payload_type  => 'websocket_msg_type'   
    );
end;
/
begin
  dbms_aqadm.create_queue
    ( queue_name          => 'websocket_queue'
    , queue_table         =>  'websocket_queue_tab'   
    );
end;
/
begin
  dbms_aqadm.start_queue 
    ( queue_name          => 'websocket_queue'
    , enqueue             => true   
    );
end;
/

-- used by the server to enqueue a message
create or replace procedure aqenq (p_direction varchar2, p_sender varchar2,p_servername varchar2, p_message clob) is
  l_enqueue_options     dbms_aq.enqueue_options_t;
  l_message_properties  dbms_aq.message_properties_t;
  l_message_handle      raw(16);
  l_ws_msg              websocket_msg_type;
begin
  l_ws_msg := websocket_msg_type(p_direction,p_sender,p_servername, p_message);  
  dbms_aq.enqueue
    ( queue_name            => 'websocket_queue'
    , enqueue_options       => l_enqueue_options
    , message_properties    => l_message_properties
    , payload               => l_ws_msg
    , msgid                 => l_message_handle
    ); 
  commit;
end;
/

-- used by the server to dequeue a message
create or replace procedure aqdeq (p_direction varchar2,p_sender out varchar2, p_servername varchar2, p_message out varchar2) is
  l_dequeue_options     dbms_aq.dequeue_options_t;
  l_message_properties  dbms_aq.message_properties_t;
  l_message_handle      raw(16);
  l_ws_msg              websocket_msg_type;
begin
  l_dequeue_options := dbms_aq.dequeue_options_t();  
  l_dequeue_options.deq_condition := 'tab.user_data.direction='''||p_direction||''' and tab.user_data.servername = '''||p_servername||'''';
  dbms_aq.dequeue
    ( queue_name => 'websocket_queue'
    , dequeue_options  => l_dequeue_options
    , message_properties  => l_message_properties
    , payload  => l_ws_msg
    , msgid  => l_message_handle
    );
  p_sender := l_ws_msg.sender;
  p_message := l_ws_msg.message;
  commit;
end;
/

 
-- example "consumer"
-- this consumer reads the message from the queue and sends a message back
-- when the received messages is 'gettime' it sends back the time
-- in the other case it sends an uppered version
declare
  l_dequeue_options     dbms_aq.dequeue_options_t;
  l_message_properties  dbms_aq.message_properties_t;
  l_message_handle      raw(16);
  l_ws_msg              websocket_msg_type;
begin
  dbms_output.enable(100000);
  l_dequeue_options := dbms_aq.dequeue_options_t();  
  l_dequeue_options.deq_condition := 'tab.user_data.direction=''todb''';
  dbms_aq.dequeue
    ( queue_name => 'websocket_queue'
    , dequeue_options  => l_dequeue_options
    , message_properties  => l_message_properties
    , payload  => l_ws_msg
    , msgid  => l_message_handle
    );
  dbms_output.put_line( 'TO:'||l_ws_msg.sender);
  dbms_output.put_line(l_ws_msg.direction);
  dbms_output.put_line(l_ws_msg.message);
  aqenq (p_direction => 'tows', p_sender => l_ws_msg.sender,p_servername => l_ws_msg.servername
    , p_message =>  case when l_ws_msg.message = 'gettime' then to_char(sysdate,'yyyy-mm-dd hh24:mi;ss') else  upper(l_ws_msg.message) end    
    );
  commit;
end;
/
