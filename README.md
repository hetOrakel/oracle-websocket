# oracle-websocket

A basic implemntation for using websockets to an Oracle Database. Written in
node. NOT FULLY FINISHED!

## howto

Install dependencies

- install Oracle Instantclient [node-oracledb install](https://oracle.github.io/node-oracledb/INSTALL.html#-3-node-oracledb-installation-instructions)
- create the objects in your Oracle Database: see configure_aq.sql
- install nodejs
- clone the repository
- in the root:

```code
  npm install
```

- copy dbconfig-example.js to dbconfig.js and edit to your needs
- start the server:

```code
   node server.js
```

- connect to the server using a test-client, for
  example [livepersonic](http://livepersoninc.github.io/ws-test-page/)
  using connection string: ws://localhost:18080

## how it works

The server is running the most basic nodejs webserver. In the database is an AQ
configured. You can use configure_aq.sql as a starting point. At startup the
server opens 2 connections to your database (edit dbconfig.js). One for reading
the queue and one for sending to the queue. 

When a client connects to WebSocket it is assigned an id. When a message is
received via the Websocket it is send to the queue using the aqenq() stored
procedure. In the database you have to build your own consumer and sender. The
last PL/SQL code block in configure_aq.sql is a very basic example. It reads the
queue and sends a message back. When the node server reads a message from the
queue the destination (the assigned id) is checked and the message will be sent
out to the corresponding connected browser.

The message from the server to the database contains a 'direction' field and the
server hostname and the assigned connection id. The consumer in the database can
use the id to send a message back to a specific client. The hostname will be
used in a multi-server setup. The 'direction' field is used to separate messages
from and to the server: tows = TO WebSocket server, todb = TO DataBase. Al
messages live in the same queue.