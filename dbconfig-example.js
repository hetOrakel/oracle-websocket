const oracledb = require('oracledb')

/*

  to use instantclient @ osx
  --------------------------
  cd $HOME/Downloads
  curl -O https://download.oracle.com/otn_software/mac/instantclient/198000/instantclient-basic-macos.x64-19.8.0.0.0dbru.dmg
  hdiutil mount instantclient-basic-macos.x64-19.8.0.0.0dbru.dmg
  /Volumes/instantclient-basic-macos.x64-19.8.0.0.0dbru/install_ic.sh
  hdiutil unmount /Volumes/instantclient-basic-macos.x64-19.8.0.0.0dbru

*/ 

oracledb.initOracleClient({libDir: '/Users/<username>/Downloads/instantclient_19_8'})

module.exports = {
    settings: {
      user          : "db-test",
      password      : "db-pass" ,
      connectString : "db-ip-or-host:1521/db-name-or-service",
    },
    db            : oracledb,
  
  }
